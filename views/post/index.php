<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Статьи';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-model-index">
<div class="row">
<div class="box">
<div class="col-lg-12">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Создать статью', ['create'], ['class' => 'btn btn-default']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary' => '',
        'columns' => [
//          ['class' => 'yii\grid\SerialColumn'],
            'id',
            'title',
            'anons:ntext',
            'content:ntext',
            'category_id',
			'publish_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
</div>
</div>
</div>
