<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PostModel */

$this->title = $model->title;
//$this->params['breadcrumbs'][] = ['label' => 'Post Models', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-model-view">
<div class="row">
<div class="box">
<div class="col-lg-12">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-default',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'anons:ntext',
            'content:ntext',
            'category_id',
            'publish_date',
        ],
    ]) ?>

</div>
</div>
</div>
</div>