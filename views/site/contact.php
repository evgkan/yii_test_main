<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

//$this->title = 'Contact';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
<!--    <h1><?= Html::encode($this->title) ?></h1> -->

    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

    <div class="alert alert-success">
        Thank you for contacting us. We will respond to you as soon as possible.
    </div>

    <p>
        Note that if you turn on the Yii debugger, you should be able
        to view the mail message on the mail panel of the debugger.
        <?php if (Yii::$app->mailer->useFileTransport): ?>
        Because the application is in development mode, the email is not sent but saved as
        a file under <code><?= Yii::getAlias(Yii::$app->mailer->fileTransportPath) ?></code>.
        Please configure the <code>useFileTransport</code> property of the <code>mail</code>
        application component to be false to enable email sending.
        <?php endif; ?>
    </p>

    <?php else: ?>

        <div class="row">
            <div class="box">
                <div class="col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">Связаться с 
                        <strong>business casual</strong>
                    </h2>
                    <hr>
                </div>
                <div class="col-md-8">
                    <!-- Embedded Google Map using an iframe - to select your location find it on Google maps and paste the link as the iframe src. If you want to use the Google Maps API instead then have at it! -->
                    <iframe width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?hl=ru&amp;ie=UTF8&amp;ll=58.5972,49.6835&amp;z=17&amp;output=embed"></iframe>
                </div>
                <div class="col-md-4">
                    <p>Phone:
                        <strong>+7 982 123 4567</strong>
                    </p>
                    <p>Email:
                        <strong><a href="mailto:name@example.com">name@example.com</a></strong>
                    </p>
                    <p>Address:
                        <strong>Россия, Кировская область,
                            <br>г.Киров</strong>
                    </p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="row">
            <div class="box">
                <div class="col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">Форма
                        <strong>для связи</strong>
                    </h2>
                    <hr>
                    <p>Если у вас есть вопросы, пожалуйста заполните следующую форму для связи с нами. Спасибо.</p>
                    <form role="form">
                        <div class="row">
                            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                            <div class="form-group col-lg-4">
                                <?= $form->field($model, 'name') ?>
                            </div>
                            <div class="form-group col-lg-4">
                                <?= $form->field($model, 'email') ?>
                            </div>
                            <div class="form-group col-lg-4">
                                <?= $form->field($model, 'subject') ?>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-lg-12">
                                <?= $form->field($model, 'body')->textArea(['rows' => 6]) ?>
                            </div>
                            <div class="form-group col-lg-6">
                                <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                                    'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                                ]) ?>
                            </div>
                            <div class="form-group col-lg-12">
                                <?= Html::submitButton('Отправить', ['class' => 'btn btn-default', 'name' => 'contact-button']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    <?php endif; ?>
</div>
