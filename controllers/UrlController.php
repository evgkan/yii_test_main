<?php

namespace app\controllers;

use Yii;
use app\models\Url;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UrlController implements the CRUD actions for Url model.
 */
class UrlController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    // Поиск соответствия в таблице URL и перенаправление
    public function actionRedirect($idurl)
    {
        $query = Url::find()
            ->where(['urlmin' => 'idurl'.$idurl])
            ->one();
        $urlmax = $query['urlmax'];
        return $this->redirect($urlmax);
    }

    /**
     * Lists all Url models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new Url();
        $dataProvider = new ActiveDataProvider([
            'query' => Url::find(),
        ]);
        if ($model->load(Yii::$app->request->post())) {
            //Генерация идентификатора
            $str='';
            for ($i=0; $i<8; $i++) {
                if (($num = rand(65,116))>90) {$num += 6;}
                $str .= chr($num);
            }
            $model['urlmin'] = 'idurl'.$str;
            $model->save();
        }
        return $this->render('index', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Deletes an existing Url model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Url model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Url the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Url::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
