<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "url".
 *
 * @property integer $id
 * @property string $urlmin
 * @property string $urlmax
 */
class Url extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'url';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['urlmin', 'urlmax'], 'required'],
            [['urlmin', 'urlmax'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'urlmin' => 'Идентификатор URL',
            'urlmax' => 'URL',
        ];
    }
}
