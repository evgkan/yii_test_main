<?php
use yii\widgets\ListView;
use yii\helpers\Html;
/* @var $this yii\web\View */
$this->title = 'Мой Блог';
?>

<div class="site-index">
    <div class="container">
        <div class="row">
            <div class="box">
                <div class="col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">Мой
                        <strong>блог</strong>
                    </h2>
                    <hr>
                </div>

<!--
                <div class="col-lg-12 text-center">
                    <img class="img-responsive img-border img-full" src="img/slide-1.jpg" alt="">
                    <h2>Post Title
                        <br>
                        <small>October 13, 2013</small>
                    </h2>
                    <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                    <a href="#" class="btn btn-default btn-lg">Read More</a>
                    <hr>
                </div>
-->


<!--    Определяем отображение ListView анонимной функцией
        переводы строк для удобства отладки
-->
                <?=ListView::widget([
                                'dataProvider' => $dataProvider,
                                'options' => [],
                                'summary' => '',
                                'itemView' => function ($model, $key, $index, $widget) {
                                    return (
                                        '<div class="col-lg-12 text-center">'."\n".
                                        '<h2>'.
                                        Html::encode($model->title)."\n".
                                        '<br><small>'.
                                        Html::encode($model->publish_date)."\n".
                                        '</small></h2><p>'.
                                        Html::encode($model->content)."\n".
                                        '</p>'."\n".
                                        '<a href="#" class="btn btn-default btn-lg">Read More</a>'."\n".
                                        '<hr>'."\n".
                                        '</div>'
                                        );
                                    },
                                ]);
                ?>
<!--
                <div class="col-lg-12 text-center">
                    <ul class="pager">
                        <li class="previous"><a href="#">&larr; Older</a>
                        </li>
                        <li class="next"><a href="#">Newer &rarr;</a>
                        </li>
                    </ul>
                </div>
-->
            </div>
        </div>

    </div>
    <!-- /.container -->
</div>
